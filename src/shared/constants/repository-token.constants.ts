export enum REPOSITORY_TOKEN {
  EVENT_REPOSITORY = 'EventRepository',
  OVERRIDE_REPOSITORY = 'OverrideRepository',
}
export const CONNECTION_NAME = 'MAIN';
