import {MigrationInterface, QueryRunner} from "typeorm";

export class SeedDbData1613587074745 implements MigrationInterface {
    name = 'SeedDbData1613587074745'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("INSERT INTO `companies` VALUES(DEFAULT, 'test company', DEFAULT, DEFAULT)");
        await queryRunner.query("INSERT INTO `users` VALUES(DEFAULT, 'test user', DEFAULT, DEFAULT)");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
