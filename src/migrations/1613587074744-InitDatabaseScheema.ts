import {MigrationInterface, QueryRunner} from "typeorm";

export class InitDatabaseScheema1613587074744 implements MigrationInterface {
    name = 'InitDatabaseScheema1613587074744'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `companies` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `overrides` (`id` int NOT NULL AUTO_INCREMENT, `override_date` datetime NOT NULL, `override_to` datetime NULL, `event_id` int NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `events` (`id` int NOT NULL AUTO_INCREMENT, `user_id` int NULL, `company_id` int NULL, `recurring_type` varchar(50) NULL, `scheduled_to` datetime NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `overrides` ADD CONSTRAINT `FK_644d090f475e4bc0b96b2d25eb2` FOREIGN KEY (`event_id`) REFERENCES `events`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `events` ADD CONSTRAINT `FK_09f256fb7f9a05f0ed9927f406b` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `events` ADD CONSTRAINT `FK_b97c36be0cf65565fad88588c28` FOREIGN KEY (`company_id`) REFERENCES `companies`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `events` DROP FOREIGN KEY `FK_b97c36be0cf65565fad88588c28`");
        await queryRunner.query("ALTER TABLE `events` DROP FOREIGN KEY `FK_09f256fb7f9a05f0ed9927f406b`");
        await queryRunner.query("ALTER TABLE `overrides` DROP FOREIGN KEY `FK_644d090f475e4bc0b96b2d25eb2`");
        await queryRunner.query("DROP TABLE `events`");
        await queryRunner.query("DROP TABLE `overrides`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `companies`");
    }

}
