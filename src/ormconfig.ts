import { ConnectionType } from './shared';
import { Event } from './modules/event/event.entity';
import { UserEntity } from './modules/user/user.entity';
import { CompanyEntity } from './modules/company/company.entity';
import { OverrideEntity } from './modules/eventOverride/override.entity';

module.exports = {
  name: ConnectionType.MAIN,
  type: 'mysql',
  extra: { connectionLimit: process.env.DB_CONNECTION_LIMIT || 100 },
  host: process.env.DB_HOST || 'localhost',
  port: parseInt(process.env.DB_PORT, 10) || 3310,
  username: process.env.DB_USERNAME || 'root',
  password: (process.env.DB_PASSWORD || '').replace('\\', '') || '123',
  database: process.env.DB_DATABASE || 'calendar',
  maxQueryExecutionTime: parseInt(process.env.DB_MAX_QUERY_EXECUTION_TIME, 10) || 1000,
  retryAttempts: parseInt(process.env.DB_RETRY_ATTEMPTS, 10) || 1000,
  entities: [Event, UserEntity, CompanyEntity, OverrideEntity],
  migrations: ['./dist/migrations/*.js'],
  cli: {
    migrationsDir: './src/migrations',
  },
  migrationsTableName: 'migrations',
  logging: process.env.DB_LOGS ? process.env.DB_LOGS.split(',') : 'error',
};
