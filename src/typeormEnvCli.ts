import { execSync } from 'child_process';

const configPath: string = './dist/ormconfig.js';

const typeormArguments: string = process.argv
  .slice(2)
  .map(args => args.replace('\\', ''))
  .join(' ');

let cmd: string;
switch (typeormArguments) {
  case 'createDB': {
    cmd = `typeorm query "CREATE DATABASE IF NOT EXISTS ${process.env.DB_NAME} CHARACTER SET utf8 COLLATE utf8_general_ci " -f ${configPath}`;
    break;
  }
  case 'dropDB': {
    if (process.env.NODE_ENV !== 'Development') {
      process.stderr.write('Drop DB can only be done on Development environment?');
      process.exit();
    }

    cmd = `typeorm query "DROP DATABASE IF EXISTS ${process.env.DB_NAME}" -f ${configPath}`;
    break;
  }
  default: {
    cmd = `typeorm ${typeormArguments} -f ${configPath}`;
    break;
  }
}

/* eslint-disable no-console */
console.log(cmd);
execSync(cmd, { env: process.env, stdio: 'inherit' });
/* eslint-enable no-console */
