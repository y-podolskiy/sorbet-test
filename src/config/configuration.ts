export default () => ({
  TCP_PORT: parseInt(process.env.TCP_PORT, 10),
  database: {
    DB_HOST: process.env.DB_HOST || 'localhost',
    DB_PORT: parseInt(process.env.DB_PORT, 10) || 3306,
    DB_DATABASE: process.env.DB_DATABASE || 'calendar',
    DB_USERNAME: process.env.DB_USERNAME || 'root',
    DB_PASSWORD: process.env.DB_PASSWORD || 123,
    DB_CONNECTION_LIMIT: parseInt(process.env.DB_CONNECTION_LIMIT, 10) || 100,
    DB_RETRY_ATTEMPTS: parseInt(process.env.DB_RETRY_ATTEMPTS, 10) || 3,
    DB_MAX_QUERY_EXECUTION_TIME: parseInt(process.env.DB_MAX_QUERY_EXECUTION_TIME, 10) || 1000,
    DB_LOGS: process.env.DB_LOGS || 'error',
  },
  IS_ACTIVE_TEST: process.env.IS_ACTIVE_TEST === 'true',


  // HISTORY_DOMAIN: process.env.DOMAIN || 'http://localhost:4220',
  HTTP_PORT: process.env.HTTP_PORT || 3000,
});
