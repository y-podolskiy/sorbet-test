import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MainModule } from './modules/main.module';

export * from '@nestjs/typeorm';

const logger = new Logger('bootstrap');

process.on('uncaughtException', (error: Error) => {
  logger.error({
    message: 'Process crashed',
    data: error.message,
  });
  setTimeout(() => {
    process.exit();
  }, 1000);
});

async function bootstrap() {
  logger.log('Prepare: start');

  const app = await NestFactory.create(MainModule);

  const configService = app.get(ConfigService);
  const httpPort = configService.get('HTTP_PORT');

  logger.log('Prepare: end');

  await app.init();

  return app.listen(httpPort);
}

bootstrap()
  .then(() => {
    logger.log('Running app: end');
  })
  .catch(error => {
    logger.error(
      {
        message: 'Running app: error',
        data: error.message,
      },
      error,
    );
  });
