import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { ERecurringType } from '../../shared';
import { CompanyEntity } from '../company/company.entity';
import { UserEntity } from '../user/user.entity';
import { OverrideEntity } from '../eventOverride/override.entity';

@Entity('events')
export class Event {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => UserEntity, user => user.events)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @Column({ name: 'user_id', nullable: true })
  public userId: number;

  @Column({ name: 'company_id', nullable: true })
  public companyId: number;

  @ManyToOne(() => CompanyEntity, company => company.events)
  @JoinColumn({ name: 'company_id' })
  company: CompanyEntity;

  @Column({ name: 'recurring_type', nullable: true, type: 'varchar', length: 50 })
  public recurringType: ERecurringType;

  @Column({ name: 'scheduled_to' })
  public scheduledTo: Date;

  @OneToMany(() => OverrideEntity, override => override.event, { cascade: ['insert']})
  overrides: OverrideEntity[];

  @CreateDateColumn({ type: 'datetime' })
  createdAt: Date;

  @CreateDateColumn({ type: 'datetime' })
  updatedAt: Date;
}
