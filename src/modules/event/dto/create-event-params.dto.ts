import { IsArray, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { OverrideEntity } from '../../eventOverride/override.entity';
import { ERecurringType } from '../../../shared';

export class CreateEventParamsDto {
  @ApiProperty()
  @IsOptional()
  @IsNumber()
  readonly companyId?;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  readonly userId?;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly recurringType?: ERecurringType;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  readonly scheduledTo;

  @ApiProperty()
  @IsOptional()
  @IsArray()
  readonly overrides?: OverrideEntity[];
}
