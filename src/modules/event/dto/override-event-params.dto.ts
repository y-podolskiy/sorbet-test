import { IsArray, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { OverrideEntity } from '../../eventOverride/override.entity';

export class OverrideEventParamsDto {
  @ApiProperty()
  @IsOptional()
  @IsArray()
  readonly overrides: OverrideEntity[];
}
