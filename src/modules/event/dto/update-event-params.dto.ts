import { IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateEventParamsDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly recurringType;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly scheduledTo;
}
