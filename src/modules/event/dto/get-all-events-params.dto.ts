import { IsDate, IsNotEmpty, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export class GetAllEventsParamsDto {
  @ApiProperty()
  @IsOptional()
  @Transform(value => Number(value))
  readonly take?: number = 50;

  @ApiProperty()
  @IsOptional()
  @Transform(value => Number(value))
  readonly skip?: number = 0;

  @ApiProperty()
  @IsNotEmpty()
  @IsDate()
  readonly from;

  @ApiProperty()
  @IsNotEmpty()
  @IsDate()
  readonly to;
}
