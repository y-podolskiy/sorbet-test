import { Inject, Injectable } from '@nestjs/common';
import { GetAllEventsParamsDto, CreateEventParamsDto, UpdateEventParamsDto, OverrideEventParamsDto } from '../dto';
import { REPOSITORY_TOKEN } from '../../../shared';
import { EventRepository } from '../event.repository';
import { Event } from '../event.entity';
import { OverrideRepository } from '../../eventOverride/override.repository';
import { OverrideEntity } from '../../eventOverride/override.entity';
import { Between } from 'typeorm';

@Injectable()
export class EventService {
  constructor(
    @Inject(REPOSITORY_TOKEN.EVENT_REPOSITORY)
    private eventRepository: EventRepository,
    @Inject(REPOSITORY_TOKEN.OVERRIDE_REPOSITORY)
    private overrideRepository: OverrideRepository,
  ) {
  }

  public async getAll(params: GetAllEventsParamsDto): Promise<Event[]> {
    const { take, skip, from, to } = params;
    return this.eventRepository.find({
      where: {
        scheduledTo: Between(from, to),
      },
      take,
      skip
    });
  }

  public async create(params: CreateEventParamsDto): Promise<Event> {
    return this.eventRepository.save(params);
  }

  public async update(id: number, params: UpdateEventParamsDto): Promise<Event> {
    return this.eventRepository.save({ id: Number(id), ...params });
  }

  public async override(id: number, params: OverrideEventParamsDto): Promise<OverrideEntity[]> {
    const overrides = params.overrides.map(ov => ({ ...ov, eventId: id }));
    return this.overrideRepository.save(overrides);
  }
}
