import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { OverrideRepository } from '../../eventOverride/override.repository';
import { EventService } from './event.service';
import { EventRepository } from '../event.repository';

describe('EventRepository', () => {
  let eventRepository: EventRepository;
  let service: EventService;

  beforeAll(
    async (): Promise<void> => {
      const moduleFixture = await Test.createTestingModule({
        providers: [
          EventService,
          EventRepository,
          OverrideRepository,
        ],
      }).compile();

      service = moduleFixture.get<EventService>(EventService);
      eventRepository = moduleFixture.get<EventRepository>(getRepositoryToken(EventRepository));
    },
  );

  beforeEach(
    () => {
      jest.restoreAllMocks();
    },
  );

  describe(
    'success read/write actions',
    () => {
      it('able to read entities', async (): Promise<void> => {
        const mockEventEntity = {
          id: 1,
          userId: 1,
          scheduledTo: new Date(),
        };
        const params = { from: '2020-01-01', to: '2020-01-02' };
        const findEvents = jest.fn().mockReturnValue(mockEventEntity);
        jest.spyOn(eventRepository, 'find')
          .mockImplementation(findEvents);
        const entities = await service.getAll(params);
         expect(findEvents).toHaveBeenCalled();
         expect(entities).toBe(mockEventEntity);
      });

      it('able to write entities', async (): Promise<void> => {
        const params = {
          userId: 1,
          scheduledTo: new Date(),
        };
        const mockEventEntity = {
          ...params,
          id: 1,
        };
        const saveEvents = jest.fn().mockReturnValue(mockEventEntity);
        jest.spyOn(eventRepository, 'save')
          .mockImplementation(saveEvents);
        const entities = await service.create(params);
        expect(saveEvents).toHaveBeenCalledWith(params);
        expect(entities).toBe(mockEventEntity);
      });
    },
  );
});
