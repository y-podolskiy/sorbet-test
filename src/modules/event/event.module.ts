import { Module } from '@nestjs/common';
import { getConnectionToken, getRepositoryToken } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { EventService } from './services/event.service';
import { EventRepository } from './event.repository';
import { REPOSITORY_TOKEN, CONNECTION_NAME } from '../../shared';
import { EventController } from './event.controller';
import { OverrideRepository } from '../eventOverride/override.repository';

@Module({
  imports: [],
  controllers: [EventController],
  providers: [
    {
      provide: REPOSITORY_TOKEN.EVENT_REPOSITORY,
      useFactory: (connection: Connection) => connection.getCustomRepository(EventRepository),
      inject: [getConnectionToken(CONNECTION_NAME)],
    },
    {
      provide: REPOSITORY_TOKEN.OVERRIDE_REPOSITORY,
      useFactory: (connection: Connection) => connection.getCustomRepository(OverrideRepository),
      inject: [getConnectionToken(CONNECTION_NAME)],
    },
    EventService,
  ],
  exports: [EventService, getRepositoryToken(EventRepository), getRepositoryToken(OverrideRepository)],
})
export class EventModule {}
