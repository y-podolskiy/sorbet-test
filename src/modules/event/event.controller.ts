import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  ValidationPipe,
  Put,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { EventService } from './services/event.service';
import { GetAllEventsParamsDto, CreateEventParamsDto, UpdateEventParamsDto, OverrideEventParamsDto } from './dto';
import { Event } from './event.entity';
import { OverrideEntity } from '../eventOverride/override.entity';

@ApiTags('Events')
@Controller('events')
export class EventController {
  constructor(private eventService: EventService) {}

  @Get('/')
  public async getAll(@Query() params: GetAllEventsParamsDto): Promise<Event[]> {
    return this.eventService.getAll(params);
  }

  @Post('/')
  public async create(
    @Body(new ValidationPipe()) dto: CreateEventParamsDto,
  ): Promise<Event> {
    return this.eventService.create(dto);
  }

  @Put('/:id')
  public async update(
    @Param('id') id: number,
    @Body(new ValidationPipe()) dto: UpdateEventParamsDto,
  ): Promise<Event> {
    return this.eventService.update(id, dto);
  }

  @Put('/:id/override')
  public async override(
    @Param('id') id: number,
    @Body(new ValidationPipe()) dto: OverrideEventParamsDto,
  ): Promise<OverrideEntity[]> {
    return this.eventService.override(id, dto);
  }
}
