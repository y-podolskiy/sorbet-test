import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConnectionType, CONNECTION_NAME } from '../../shared';
import { configOption, getConfig } from '../../main.config';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      name: CONNECTION_NAME,
      useFactory: (configService: ConfigService) => getConfig(ConnectionType.MAIN, configService.get('database') as configOption),
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
