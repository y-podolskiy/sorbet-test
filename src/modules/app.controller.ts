import { Controller, Get } from '@nestjs/common';

@Controller('/')
export class AppController {
  @Get('ping')
  public async ping() {
    return { stauts: 'pong' };
  }
}
