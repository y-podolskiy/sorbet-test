import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Event } from '../event/event.entity';

@Entity('overrides')
export class OverrideEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ name: 'override_date' })
  public overrideDate: Date;

  @Column({ name: 'override_to', nullable: true })
  public overrideTo: Date;

  @Column({ name: 'event_id', nullable: true })
  public eventId: number;

  @ManyToOne(() => Event, event => event.overrides )
  @JoinColumn({ name: 'event_id' })
  public event: Event;

  @CreateDateColumn({ type: 'datetime' })
  public createdAt: Date;
}
