import { EntityRepository, Repository } from 'typeorm';
import { OverrideEntity } from './override.entity';

@EntityRepository(OverrideEntity)
export class OverrideRepository extends Repository<OverrideEntity> {
}
