import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import configuration from '../config/configuration';
import { DatabaseModule } from './database/database.module';
import { AppController } from './app.controller';
import { EventModule } from './event/event.module';

@Module({
  imports: [
    ConfigModule.forRoot({ load: [configuration], isGlobal: true }),
    DatabaseModule,
    EventModule,
  ],
  controllers: [AppController],
})
export class MainModule {}
