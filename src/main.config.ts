import * as Joi from 'joi';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConnectionType } from './shared';

const ormconf = require('./ormconfig');

export * from '@nestjs/typeorm';
export * from 'typeorm';
export * from 'typeorm/driver/mysql/MysqlDriver';
export * from 'typeorm/error/EntityNotFoundError';

export type configOption = {
  DB_CONNECTION_LIMIT: number;
  DB_HOST: string;
  DB_PORT: number;
  DB_USERNAME: string;
  DB_PASSWORD: string;
  DB_DATABASE: string;
  DB_MAX_QUERY_EXECUTION_TIME: number;
  DB_RETRY_ATTEMPTS: number;
  DB_LOGS?: string;
};

const envHistoryJoi = {
  DB_HOST: Joi.string().required(),
  DB_PORT: Joi.number().default(3306),
  DB_CONNECTION_LIMIT: Joi.number().required(),
  DB_MAX_QUERY_EXECUTION_TIME: Joi.number().default(200),
  DB_USERNAME: Joi.string().required(),
  DB_PASSWORD: Joi.string().required(),
  DB_DATABASE: Joi.string().required(),
  DB_LOGS: Joi.string().default('query'),
};

const envVarsSchemaHistory: Joi.ObjectSchema = Joi.object(envHistoryJoi);

export function getConfig(
  type: ConnectionType,
  config?: configOption,
): TypeOrmModuleOptions {
  const proceedConf = (error, validatedEnvConfig) => {
    if (error) {
      throw new Error(`Config vali dation error: ${error.message}`);
    }

    if (config) {
      const entries = Object.entries(validatedEnvConfig);
      for (const [key, value] of entries) {
        process.env[key] = <string>value;
      }
    }
  };

  const { error, value: validatedEnvConfig } =  envVarsSchemaHistory.validate(config || process.env,  {
    stripUnknown: true,
  })
  console.log(error,validatedEnvConfig)
  proceedConf(error, validatedEnvConfig);

  return ormconf;
}
