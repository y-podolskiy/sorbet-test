docker build -t service-calendar -f dockerfile.dev ./ \
docker-compose up \
npm run typeorm:cli migration:run

GET http://localhost:8898/ping

GET http://localhost:8898/events/
query params: from, to, take, skip

POST http://localhost:8898/events/
body: `{
"companyId: 1,
"userId: 1,
"scheduledTo": "2020-01-01",
"recurringType": "daily",
"overrides": [
{
"overrideDate": "2021-10-21",
"overrideTo": "2020-01-22"
}
]
}`

PUT http://localhost:8898/events/1
body: `{
"scheduledTo": "2020-01-01",
"recurringType": "daily"
}`

PUT http://localhost:8898/events/1/override
`{
"overrides": [
"overrideDate": "2021-10-21",
"overrideTo": "2020-01-22"
]
}`
